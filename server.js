const express = require("express");
const todoRoutes = require("./src/todo/routes");
const errorHandler = require("./errorHandler");
const app = express();
const port = process.env.PORT || 5050;

app.use(express.json());

app.get("/", (req, res) => {
  res.send("Just changed the hello world to something...");
});
app.use("/todos", todoRoutes);

app.use(errorHandler.error505Handler);

app.listen(port, (error) => {
  console.log("listening on port " + port);
});
