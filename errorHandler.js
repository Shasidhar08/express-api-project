const error505Handler = (error, request, response, next) => {
  res.status(500).json({ message: error.message });
};

module.exports = { error505Handler };
