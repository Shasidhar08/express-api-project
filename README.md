# INSTRUCTIONS:

## Pre-requisites for the project

- Node.js
- PostgreSQL

## Create the database

- Create a database in the postgres

### Clone the repo From Gitlab

```bash git
git clone git@gitlab.com:Shasidhar08/express-api-project.git
```

## After Cloning

**open the project and run following commands in the terminal in the project directory**

```bash
npm install #installs all the dependencies  # Prisma, Express, Superstruct
```

## Create and configure .env file

- Create a file in the project root called .env
- Copy DATABASE_URL envirinment variable in .env.example to the .env file
- Modify the DATABASE_URL in .env with instructions as mentioned in the .env.example

## Run the following command to add table to your database:

```bash
npx prisma migrate
```

5. Run the server :

```bash
node server.js  #Needs to be rerun to make changes after every save
#OR
npm run dev   # It automatically restarts after save while server is up and running.
```
