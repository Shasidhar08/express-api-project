const { Router } = require("express");
const controller = require("./controller");
const errorHandler = require("../../errorHandler");
const router = Router();

router.get("/", controller.getTodos);
router.get("/:id", controller.getTodoById);
router.post("/", controller.addTodo);
router.delete("/:id", controller.removeTodo);
router.put("/:id", controller.updateTodo);

// router.put("*", (req, res) => {
//   res.status(404).json({ message: error.message });
// });
module.exports = router;
