const { assert } = require("superstruct");

const { PrismaClient } = require("@prisma/client");
const validate = require("./utils");
const prisma = new PrismaClient();

const getTodos = async (req, res) => {
  try {
    const data = await prisma.todo.findMany();
    res.status(200).json(data);
  } catch (error) {
    res.status(500).json({ message: "Internal Server Error" });
  }
};

const getTodoById = async (req, res, next) => {
  try {
    let paramsId = parseInt(req.params.id);
    assert({ id: paramsId }, validate.idCheck);
    let result = await prisma.todo.findUnique({
      where: {
        id: paramsId,
      },
    });
    if (result === null) {
      res.status(404).json({ message: "Not Found" });
    } else {
      res.status(200).json(result);
    }
  } catch (error) {
    next(error);
  }
};

const addTodo = async (req, res, next) => {
  try {
    assert(req.body, validate.addTodoCheck);
    const result = await prisma.todo.create({
      data: {
        text: req.body.text,
        iscompleted: req.body.iscompleted,
      },
    });
    if (!result) {
      res.status(400).json({ Message: err.meta.cause });
    }
    res.status(201).json(result);
  } catch (error) {
    next(error);
  }
};

const removeTodo = async (req, res, next) => {
  try {
    let id = parseInt(req.params.id);

    assert({ id }, validate.idCheck);

    let result = await prisma.todo.findUnique({
      where: {
        id,
      },
    });

    if (result === null) {
      res.status(404).json({ message: "Not Found" });
    }

    const deleteTodo = await prisma.todo.delete({
      where: {
        id: parseInt(req.params.id),
      },
    });
    res.status(200).json(deleteTodo);
  } catch (error) {
    next(error);
  }
};

const updateTodo = async (req, res, next) => {
  try {
    let id = parseInt(req.params.id);
    assert({ id, ...req.body }, validate.updateTodoCheck);
    let todo = await prisma.todo.findUnique({
      where: {
        id,
      },
    });

    if (todo === null) {
      res.status(404).json({ message: "Not Found" });
    }

    const result = await prisma.todo.update({
      where: {
        id: parseInt(req.params.id),
      },
      data: {
        text: req.body.text,
        iscompleted: req.body.iscompleted,
      },
    });

    if (!result) {
      res.status(404).json({ message: err.meta.cause });
    }
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
};

module.exports = { getTodos, getTodoById, addTodo, removeTodo, updateTodo };
