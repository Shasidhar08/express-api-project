const { object, string, number, boolean, defaulted } = require("superstruct");

const idCheck = object({
  id: number(),
});

const addTodoCheck = object({
  text: string(),
  iscompleted: defaulted(boolean(), false),
});

const updateTodoCheck = object({
  id: number(),
  text: string(),
  iscompleted: defaulted(boolean(), false),
});

module.exports = { addTodoCheck, updateTodoCheck, idCheck };
